#!/usr/bin/perl

require 5.006;
use strict;
use warnings;

my $tiarra_version = `./tiarra --dumpversion`;
chomp $tiarra_version;
my $package_version = `dpkg-parsechangelog | sed -n -e '/^Version:/s/Version: //p'`;
chomp $package_version;

(my $package_revision = $package_version) =~ s/^[^-]*-//;

my %defs = (
    version => 'debian-'.$package_version,
    based_version => $tiarra_version,
    short_version => $tiarra_version.'+debian-'.$package_revision,
   );

my $defs = join('|', map { quotemeta } keys %defs);

my $oldargv;
while (<>) {
    if (!defined $oldargv || $ARGV ne $oldargv) {
	my ($mode, $uid, $gid) = (stat($ARGV))[2,4,5];
	unlink $ARGV;
	open(ARGVOUT, ">$ARGV");
	chmod $mode, $ARGV;
	chown $uid, $gid, $ARGV;
	select ARGVOUT;
	$oldargv = $ARGV;
    }
    s/^(\s*my\s+\$($defs)\s*=\s*).+(;\s*)$/$1.'"'.quotemeta($defs{$2}).'"'.$3/e;
} continue {
    print;
}
select STDOUT;
